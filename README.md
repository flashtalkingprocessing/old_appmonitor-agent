##System Monitor for App Monitor

###Installation

Local Installation

    curl -fsSLo appmonitor-agent.phar https://bitbucket.org/flashtalkingprocessing/appmonitor-agent/src/master/build/appmonitor-agent.phar
    chmod a+x appmonitor-agent.phar

Call directly

    ./appmonitor-agent.phar start serverid hostendpoint  --options
    
Global Installation

    curl -fsSLo appmonitor-agent.phar https://bitbucket.org/flashtalkingprocessing/appmonitor-agent/src/master/build/appmonitor-agent.phar
    chmod a+x appmonitor-agent.phar
    sudo mv appmonitor-agent.phar /usr/bin/appmonitor-agent

Call Anywhere

    appmonitor-agent start serverid hostendpoint  --options

###Help

For a list of available commands

    appmonitor-agent

To see the help for any listed command add the `--help` option

    appmonitor-agent start --help
    appmonitor-agent stop --help

###Update

The agent is capable of performing a self update

    appmonitor-agent update
 
 > NB: You must stop and restart the agent using the `appmonitor-agent stop` command, followed by `appmonitor-agent start  serverid hostendpoint  --options`


###Monit Configuration

The best way of running the daemon is by installing monit `yum install monit` and dropping the following content into `/etc/monit.d/appmonitor-agent.conf`

Note the -a parameters are a method of passing custom parameters to the endpoint. --secure (-s) will force communication over HTTPS.

~~~
check process appmonitoragent with pidfile /var/run/appmonitor-agent.pid
  start program = "/usr/bin/appmonitor-agent start <SERVER_ID> <HTTP_API_ENDPOINT> -a <PARAM:VALUE> -a <PARAM:VALUE> --secure"
  stop program = "/usr/bin/appmonitor-agent stop"
~~~